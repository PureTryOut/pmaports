# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-fairphone-fp2
pkgdesc="Fairphone 2"
pkgver=2
pkgrel=15
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="postmarketos-base mkbootimg"
makedepends="devicepkg-dev"
source="deviceinfo
	FP2.conf
	hifi
	rootston.ini
"
subpackages="
	$pkgname-kernel-downstream:kernel_downstream
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-downstream-firmware:downstream_firmware
	$pkgname-mainline-firmware:mainline_firmware
	$pkgname-alsa
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_downstream() {
	pkgdesc="Downstream kernel"
	depends="linux-fairphone-fp2 mesa-dri-swrast ofono"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel"
	depends="linux-postmarketos-qcom-msm8974 mesa-dri-freedreno"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="Firmware for WiFi, Audio, etc."
	depends="firmware-fairphone-fp2-adsp firmware-fairphone-fp2-modem firmware-fairphone-fp2-wcnss"
	mkdir "$subpkgdir"
}

downstream_firmware() {
	pkgdesc="Firmware support packages for downstream kernel"
	depends="$pkgname-alsa adsp-audio msm-modem-downstream wcnss-wlan"
	install_if="$pkgname-kernel-downstream $pkgname-nonfree-firmware"
	mkdir "$subpkgdir"
}

mainline_firmware() {
	pkgdesc="Firmware support packages for mainline kernel"
	depends="msm-modem-rpmsg firmware-adreno"
	install_if="$pkgname-kernel-mainline $pkgname-nonfree-firmware"
	mkdir "$subpkgdir"
}

alsa() {
	pkgdesc="Audio configuration files"

	mkdir -p "$subpkgdir"/usr/share/alsa/ucm/FP2
	install -Dm644 "$srcdir"/FP2.conf "$subpkgdir"/usr/share/alsa/ucm/FP2/
	install -Dm644 "$srcdir"/hifi "$subpkgdir"/usr/share/alsa/ucm/FP2/
}

phosh() {
	install_if="$pkgname postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="f594853f973525c59c689d1f46d9be9005e05e170339daaca6d06f07a4a60d0de3db47785aebb35bdb2b129fae6bdd6c32f92e986e9fd09db07aba4534156ef2  deviceinfo
55360b1ba4ddadea341c9edb13c32ba5f19aabf75ab28602f30cfb79b9df8834f115ef979c70569f23ec1293b1fcd408baf320d87803293ce7106bdc73a26c9f  FP2.conf
b834461c6866bb0c473dd089dd5da641dd42a00f610aad6503117aa50fe6e200db9ad0a264bb609f12350d59faee1f772907bacd75439f702ea7d52f6f85e2b1  hifi
aad7cce10db5ec156585893c82932bd42f2c86e4d68208b5f3816ac7f5f80eb6ddf15157666e81de97d30b20897f88e1027545a87fe5c96c37b5e75e3e125ee4  rootston.ini"
