pkgname=linux-samsung-i9100-mainline
pkgver=5.8.12
pkgrel=0
pkgdesc="Samsung Galaxy SII mainline kernel"
arch="armv7"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="perl sed flex bison openssl-dev installkernel bash gmp-dev bc linux-headers elfutils-dev devicepkg-dev busybox-static-$arch"

_carch="arm"
_flavor="samsung-i9100"
_config="config-$_flavor.$arch"
source="
	$pkgname-$pkgver.tar.xz::https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$pkgver.tar.xz
	$_config
	init
	initramfs.list
	01_flush_tlb.patch
"
builddir="$srcdir/linux-$pkgver"

prepare_isorec() {
	_initramfsdir="$builddir/usr/i9100"
	mkdir -p "$_initramfsdir"
	cp -v /usr/$(arch_to_hostspec $arch)/bin/busybox.static "$_initramfsdir"
	cp -v "$srcdir"/init "$_initramfsdir"
	cp -v "$srcdir"/initramfs.list "$_initramfsdir"
}

prepare() {
	default_prepare
	prepare_isorec
	cp -v "$srcdir"/$_config .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOADADDR=0x40008000
}

package() {
	cat "$builddir/arch/arm/boot/zImage" \
		"$builddir/arch/arm/boot/dts/exynos4210-i9100.dtb" \
		> "$builddir/arch/arm/boot/zImage-dtb"

	install -Dm644 "$builddir/arch/arm/boot/zImage-dtb" \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir"
}

sha512sums="c1daa20dcdaa2cb805e7a973b684ea40327088d8fadf0cf44f4c73284923363397b12f2e1958d64d6a0ff348198366d46b59a5301a72d3431389d319a4bde489  linux-samsung-i9100-mainline-5.8.12.tar.xz
f09d6c61f6f5573fc7512bab4b6b8ccf66ce6edf2c3783fbc05ab8aff142aae2b3fe0b8ad1085b8412f0664d0601362a79b43a502ff5e98de9de756b7b88a979  config-samsung-i9100.armv7
1181e84aec07a1eb2d9f1491581893b1de8535abf784115ea5666891653b3c2f4ed062a4a9cf80e07731bc1553d30ff72601c64d966b33b669cc569f70d61610  init
6a9a629805b5601db0183a796b13baae0fb2344ad24ef8ea55ea47ede8ee50a299c131d1862e15ed2bd3709aaf97f4f05633635b5764a83f4564c75cd5941967  initramfs.list
f1368045393d9be596154099ef08833e8640005a88cbefb741e57be29440de263bf63bf9645a176bce6d3fd4e3ac7daf11433ee937064f792f61dd54d7339f06  01_flush_tlb.patch"
