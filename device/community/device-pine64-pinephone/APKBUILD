# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Martijn Braam <martijn@brixit.nl>
# Co-Maintainer: Luca Weiss <luca@z3ntu.xyz>
# Co-Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=device-pine64-pinephone
pkgver=0.10
pkgrel=3
pkgdesc="PINE64 PinePhone"
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware $pkgname-phosh"
depends="postmarketos-base u-boot-pinephone uboot-tools mesa-dri-gallium linux-postmarketos-allwinner gpsd atinout alsa-ucm-conf"
makedepends="devicepkg-dev"
install="$pkgname.post-install"
source="deviceinfo
	uboot-script.cmd
	sysrq.conf
	hwtest.ini
	10-pinephone-proximity.rules
	gpsd_pinephone.initd
	gpsd_device-hook.sh
	90-modem-eg25.rules
	ucm/PinePhone.conf
	ucm/HiFi.conf
	ucm/VoiceCall.conf
	eg25.initd
	setup-modem.sh
	"
# workaround to purge the -elogind subpackage that was previously
# created/installed
provides="$pkgname-elogind=$pkgver-r$pkgrel"

build() {
	devicepkg_build $startdir $pkgname
	mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n postmarketos -d "$srcdir"/uboot-script.cmd "$srcdir"/boot.scr
}

package() {
	devicepkg_package $startdir $pkgname
	install -Dm644 "$srcdir"/boot.scr \
		"$pkgdir"/boot/boot.scr

	# Make /boot/allwinner/sun50i-a64-pine64-pinephone*.dtb resolve to /boot/sun50i-a64-pine64-pinephone*.dtb
	# this is because the device tree names in u-boot include the vendor and the one in postmarketOS doesn't
	ln -s .. "$pkgdir"/boot/allwinner

	install -Dm644 "$srcdir"/hwtest.ini \
		"$pkgdir"/usr/share/hwtest.ini

	# GPS
	install -Dm755 "$srcdir"/gpsd_pinephone.initd \
		"$pkgdir"/etc/init.d/gpsd_pinephone
	install -Dm755 "$srcdir"/gpsd_device-hook.sh \
		"$pkgdir"/etc/gpsd/device-hook
	install -D -m644 "$srcdir"/10-pinephone-proximity.rules \
		"$pkgdir"/usr/lib/udev/rules.d/10-pinephone-proximity.rules

	# Fix "sysrq: HELP..." messages in dmesg while playing audio
	# (Headphone output interferes with the serial console on the headphone jack)
	install -Dm644 "$srcdir"/sysrq.conf \
		"$pkgdir"/etc/sysctl.d/sysrq.conf

	# Alsa usecase manager config
	install -Dm644 "$srcdir"/PinePhone.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/PinePhone.conf
	install -Dm644 "$srcdir"/HiFi.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/HiFi.conf
	install -Dm644 "$srcdir"/VoiceCall.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/VoiceCall.conf

	# Modem
	install -Dm755 "$srcdir"/eg25.initd "$pkgdir"/etc/init.d/eg25
	install -Dm755 "$srcdir"/setup-modem.sh \
		"$pkgdir"/usr/bin/pinephone_setup-modem
	install -Dm644 "$srcdir"/90-modem-eg25.rules -t "$pkgdir"/usr/lib/udev/rules.d/

}

nonfree_firmware() {
	pkgdesc="Wifi, Bluetooth and Autofocus firmware"
	depends="linux-firmware-rtlwifi linux-firmware-rtl_bt firmware-pine64-rtl8723bt firmware-pine64-ov5640"
	mkdir "$subpkgdir"
}

phosh() {
	install_if="$pkgname postmarketos-ui-phosh"
	depends="wys-pinephone"
	mkdir "$subpkgdir"
}

sha512sums="c862454638db61f998c72848073bb7c97ccc51ac689865c5aa201d5e93a757b3d84c3807c3e5300ea0c3bd8adf0471e5148ad45ee203a690d4b909e7b90becbb  deviceinfo
b20d4fb9f08a1bbc1c12ce6940e438a00f5c8d400fbc5071e951ffc69f77d2421204472e86f1a7df0488d250f3ec16205b75d4eac8c3cb5521fe9a02ea24865a  uboot-script.cmd
f4b5509fd6a8b23f3667f5e7262b3a19c607a37cb9eaf7d0e93eb826d45c26ec12df4810879bacb8e4042bb83cc80b2b436224c8d47b6d67361369a724bbf7ee  sysrq.conf
3dd6d612c381cb0002049d1974d8fb5aa5a53a1eb4d6bcbf62eb2ad52cfdc45f0f6ad24a699716d3513b0371aa1316f25dc72afc10d7176cc3b99d0965c3f030  hwtest.ini
b53cc6f2531854cc9c1e4c334185a20551d64c8675ee8a8eaa03b99d80808fad421a0f6e99e5be212a974d88c85f461a71ba59ac59c29f298c82f211e3be1ef4  10-pinephone-proximity.rules
1017fc3f325227da58c77abdc59e9735288d91a7ccc63ec784fe0241c523786b617a11ce8045dab2a74ca12a7dd70aaa334af91836418db1e96a3266fecaa4fd  gpsd_pinephone.initd
ccbc83b84b5028bc2c8e526759004ce71b50b2675ecffee98f5676c70a3332197a231ff9d2fd46444dd3c0a637ec08ce6125b18240fe6bfc13f624a15192e648  gpsd_device-hook.sh
7dc2b7c20b4a2b15f597a6417bd01797643dad84a3683b0dee648e030fb6326e9d020307643fdcdf1bb43fc44af9975697e417003bd359610bae2d8ce614fc00  90-modem-eg25.rules
e852b48a687f9b2a0eca444aa3d00a1818aead9f5e5d28e070b51c9d6f8ec648e66f1d88e2bfa94d74533f9ffb9aacc1703da2a06693f85fa04ff97fd7528012  PinePhone.conf
c3abc45269fb135075f5e227161c68ac234775b87fcf224385c5f40123108bf87e1351a1f1ec6786e964f493129511b8363070116a9a451e53cd7b17b3a76cc8  HiFi.conf
5c12e9bf4677bbcf49c16a731f7580bb18df95489b10671323dc2f600bc8e1be0031fd911db47ad641500a7bb58d9386fa124c63456a076fddda796d0d165b1d  VoiceCall.conf
db57bd1613ae988a05df86fa962352145f480e1a4e5a0ff921e734cf35ecd755b7f14775a70bb31a7e875813cbc7a4722202cf76023f1b15862bbd5390d0b757  eg25.initd
0c81d758e1bcb56ed2cdaf91124121ebbd4dd7a5e25f02a7685b837faf660949d05f6b07b39a1c6a9ca22a7029cdcf3c6dac8f1038e37c8a34cb7c5702e9df51  setup-modem.sh"
