# Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=soc-qcom-msm8916
pkgdesc="Common package for Qualcomm MSM8916 devices"
pkgver=9
pkgrel=0
url="https://postmarketos.org"
license="BSD-3-Clause"
arch="aarch64 armv7"
options="!check !archcheck !tracedeps pmb:cross-native"
depends="mesa-dri-gallium $pkgname-ucm"
subpackages="$pkgname-ucm $pkgname-modem"

_ucm_commit="c34a73dc0714834d2d2ff5ae304e20b7d93c4c98"
source="$pkgname-$_ucm_commit.tar.gz::https://github.com/msm8916-mainline/alsa-ucm-conf/archive/$_ucm_commit.tar.gz
	q6voiced.conf
	"

package() {
	# parent package is empty
	mkdir -p "$pkgdir"
}

# Upstream keeps making breaking changes to UCM in patch releases.
# My last upstreaming efforts failed without ever getting a reply,
# since then UCM was entirely reworked like 3 times already...
# I don't want to fix this up every few months, so let's package a stable
# version for now. Once all the UCM refactoring upstream has settled down a bit
# we can investigate how to integrate it properly for upstreaming.
ucm() {
	provides="alsa-ucm-conf"

	cd "$srcdir/alsa-ucm-conf-$_ucm_commit"
	mkdir -p "$subpkgdir"/usr/share/alsa
	cp -r ucm2 "$subpkgdir"/usr/share/alsa
}

modem() {
	depends="msm-modem-rpmsg q6voiced"
	install="$subpkgname.post-install"

	install -Dm644 q6voiced.conf "$subpkgdir"/etc/conf.d/q6voiced
}

sha512sums="498a6303dd3b9e2467f8a2935dd49a261334cc87d43c65c87f0b82a4c23db44ec1ff9f5891b93f30942a8077ef2a80638a5095e71af299cc359e857e81829e4c  soc-qcom-msm8916-c34a73dc0714834d2d2ff5ae304e20b7d93c4c98.tar.gz
3a4a9322839d4b3ef9d79668a37840a9f444954759ae3c512e694051d2f9a2573db42ad6c4c1a5c75eeb861232a27ba1a8cef9b503decd54ead25a96e3dd6f98  q6voiced.conf"
